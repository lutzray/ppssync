#include <MicroNMEA.h>
// adapted from https://github.com/stevemarple/MicroNMEA/tree/master/examples/demo
// Requires MicroNMEA, a compact Arduino library to parse a subset of NMEA sentences.
//
// Flashes onboard LED and prints out UTC time (DD HH MM SS) through a hardware interupts
// from the GPS 1PPS signal. This event is processed in the loop() fct to toggle the LED
// and print the time once a second. Otherwise, the loop() polls the serial port and build up
// the GPS sentence.
// NB: the time stored in the UART buffer is for the previous PPS pulse so it's one sec late.

#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
// vendor specific command string,  see https://cdn-shop.adafruit.com/datasheets/PMTK_A11.pdf
// or search for "PMTK Globaltop"

// Refer to serial devices by use
#define console Serial
#define gps  Serial1

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
// [todo] should check UART buffer size too
// because we won't be polling serial so frequently when doing ASK Direct Digital Synthesis
// [done]: from Arduino15/packages/adafruit/hardware/samd/1.6.4/cores/arduino/RingBuffer.h :
// #define SERIAL_BUFFER_SIZE 350
bool ledState = LOW;
volatile bool ppsTriggered = false;

void ppsHandler(void);

void ppsHandler(void)
{
  ppsTriggered = true;
}

void printUnknownSentence(MicroNMEA& nmea)
{
  console.println();
  console.print("Unknown sentence: ");
  console.println(nmea.getSentence());
}

void setup(void)
{
  console.begin(9600); // console
  gps.begin(9600); // gps
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, ledState);
  nmea.setUnknownSentenceHandler(printUnknownSentence);
  MicroNMEA::sendSentence(gps, PMTK_SET_NMEA_OUTPUT_RMCONLY); // asks only $GPRMC
  attachInterrupt(digitalPinToInterrupt(2), ppsHandler, RISING);
}

void loop(void)
{
  if (ppsTriggered) {
    ppsTriggered = false;
    ledState = !ledState;
    digitalWrite(LED_BUILTIN, ledState);

    // Output GPS information from previous second

    console.print(nmea.getDay());
    console.print(" ");
    console.print(int(nmea.getHour()));
    console.print(" ");
    console.print(int(nmea.getMinute()));
    console.print(" ");
    console.println(int(nmea.getSecond()));
    nmea.clear();
  }

  while (!ppsTriggered && gps.available()) {
    char c = gps.read();
//    console.print(c); //uncomment to echo GPS output to Serial Monitor
    nmea.process(c);
  }

}
