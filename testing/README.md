# Kicking the tires
Some sketches to incrementaly learn and test hardware and APIs, doing baby steps :

* `SerialPassthrough.ino`, straight from Arduino Examples. This will validates UART connection between the GPS module and the Adafruit Trinket M0. Open the *Serial Monitor* and check for $GPRMC sentences from the GPS module.
* `BlinkPPS.ino` flashes onboard LED and prints out UTC time through hardware interupts. The GPS [1PPS signal](https://en.wikipedia.org/wiki/Pulse-per-second_signal) cabled to the Trinket [pin 2](https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino/pinouts) triggers an external interrupt configured with an
  `attachInterrupt(digitalPinToInterrupt(2), ppsHandler, RISING)`. `ppsHandler()` beeing the Interrupt Service Routine (ISR) aka interrupt handler. Adapted from a [MicroNMEA](https://github.com/stevemarple/MicroNMEA) example, a compact Arduino library to parse a subset of NMEA sentences.
*  `PulsedSyncBit.ino` modulates a short sin pulse on rising edge of 1PPS
*  `RawUTC.ino` outputs (through the DAC) the sinusoid Sync bit with UTC DDHHMMSS in binary digital (square) output.
*  *Finally* `PPSSync.ino` outputs (through the DAC) the sinusoid Sync bit with ASK/BFSK modulated UTC DDHHMMSS.
