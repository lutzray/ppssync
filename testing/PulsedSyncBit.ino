// This sketch modulates a short sin pulse with Direct Digital Synthesis on the SAMD21 DAC pin,
// trigered by an external interrupt: the rising edge of an 1PPS GPS signal.
// See https://hackaday.io/project/176196-atomic-synchronator for context.
//
// It is loosely based on forcetronics ZeroWaveGen sketch
// http://forcetronic.blogspot.com/2015/10/arduino-zero-dac-overview-and-waveform.html
// https://www.instructables.com/Arduino-Zero-DAC-Overview-and-Waveform-Generator-E/
// http://www.forcetronics.com/
// This code is free and open for anybody to use and modify at their own risk

#define DEBUG

#define gps        Serial1
#define console    Serial
// Refer to serial devices by use

#define out_DAC_sampling_period 1 // in microseconds
#define nbr_of_whole_cycles_in_sync_bit 2
#define desired_sync_bit_frequency 1500 // in Hertz
#define presumed_audio_rec_sampling_freq 48000 // in Hertz

int n_values_in_sync_bit_cycle = 1e6 / (desired_sync_bit_frequency*out_DAC_sampling_period);
// so a repeated cycle starts straight on a sample, avoiding discontinuities
float effective_sync_bit_frequency = 1e6 / (n_values_in_sync_bit_cycle*out_DAC_sampling_period);
// because the signal period is constrained to be an integer multiple of the sampling period,
// the signal frequency won't exactly equal the frequency value stored in desired_sync_bit_frequency
// so we evaluate it (for debugging purpose only).
bool ledState = LOW;
volatile bool ppsTriggered = false; // flag to detect the external interrupt in loop()
int *wavSamples; // array to store sinewave points

void ppsHandler(void)
// Interrupt Service Routine (ISR) for the 1PPS, attached in setup()
{
  ppsTriggered = true;
}

void setup(void) {
  Serial.begin(9600);
  // pinMode(A0, OUTPUT);      NO NO NO... pin A0 would be set as a digital output and this would interfere with the dac output
  // see https://forum.arduino.cc/index.php?topic=525853.msg3589365#msg3589365
  analogWriteResolution(10); // set the DAC for 10 bits of resolution (max)
  wavSamples = (int *) malloc(n_values_in_sync_bit_cycle * sizeof(int)); // Allocate the buffer where the samples are stored
  attachInterrupt(digitalPinToInterrupt(2), ppsHandler, RISING); // silkscreened pin 2 on the Adafruit Trinket M0
  genSin(n_values_in_sync_bit_cycle); // generates and store the sine in wavSamples

#ifdef DEBUG
  while (!Serial);
  console.print("microseconds between DAC samples: ");
  console.println(out_DAC_sampling_period);
  console.print("presumed audio recorder sampling freq: ");
  console.print(presumed_audio_rec_sampling_freq);
  console.println(" Hz");
  console.print("SAMD21 oversampling ratio (nbr of DAC samples for one audio rec sample): ");
  console.println(1e6 / (presumed_audio_rec_sampling_freq * out_DAC_sampling_period));
  console.print("desired freq: ");
  console.println(desired_sync_bit_frequency);
  console.print("effective_sync_bit_frequency: ");
  console.println(effective_sync_bit_frequency);
  console.print("nb of values per cycle: ");
  console.println(n_values_in_sync_bit_cycle);
  console.print("nb of cycles for sync bit: ");
  console.println(nbr_of_whole_cycles_in_sync_bit);
#endif
}

void loop(void)
{
  if (ppsTriggered) {
    ppsTriggered = false;
    for (int j = 0; j < nbr_of_whole_cycles_in_sync_bit; j++) {
      for (int i = 0; i < n_values_in_sync_bit_cycle; i++) {
        int value = wavSamples[i];
        analogWrite(A0, value);
        delayMicroseconds(out_DAC_sampling_period);
#ifdef DEBUG
        console.println(value);
#endif
      }
    }
  }
}

// This function generates a sine wave and stores it in the wavSamples array
// The input argument is the number of points the sine wave is made up of.
// Boundaries are chosen so it's Sin(0) for the first sample and Sin(2pi) for last sample + 1
// then multiple repetitions can be joined without discontinuities
void genSin(int sCount) {
  const float pi2 = 6.28; //2 x pi
  float phase;

  for (int i = 0; i < sCount; i++) { // loop to build sine wave based on sample count
    phase = pi2 * (float)i / (float)sCount; // calculate value in radians for sin()
    wavSamples[i] = ((int)(sin(phase) * 511.5 + 511.5)); // Calculate sine wave value and offset based on DAC resolution 511.5 = 1023/2
  }
}
