# ppssync

SAMD21 code for the [AtomicSynchronator](https://hackaday.io/project/176196-atomic-synchronator). More details on the [Wiki](https://gitlab.com/lutzray/ppssync/-/wikis/home).
