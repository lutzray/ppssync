#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from math import pi,sin
import numpy as np
import matplotlib.pyplot as plt


f_sig1 = 600 # Hz, lower frequency
symbol_duration = 0.02 # second
# symbol_duration = 10e-3 # second
ra, rb = (7, 4)
f2_f1_ratio = float(ra)/rb

f_sample = 48e3 # Hz
sampling_period = 1/f_sample

NSamples = round(symbol_duration/sampling_period)
mode = 1
sig1_period = f_sample / f_sig1
mode_period = NSamples / mode
while (mode_period > sig1_period):
    mode_period = round(NSamples / mode)
    # print(mode, mode_period, sig1_period)
    mode += 1

mode -= 1
eff_f1 = (1/symbol_duration)*mode

msg = '\neffective f1: %.2f Hz with %i samples'
print(msg%(eff_f1, NSamples))

print('checks for f1:')
print('  %i sig1 cycles takes %.3f ms'%(mode, 1e3*(1/eff_f1)*mode))
print('  symbol ends after:  %.3f ms'%(1e3*NSamples/f_sample))
print('  audio sample each %.3f ms'%(1e3/f_sample))

goal_f2 = eff_f1 * f2_f1_ratio

mode = 1
sig2_period = f_sample / goal_f2
mode_period = NSamples / mode
while (mode_period > sig2_period):
    mode_period = round(NSamples / mode)
    # print(mode, mode_period, sig2_period)
    mode += 1

eff_f2 = (1/symbol_duration)*mode
msg = '\neffective f2: %.2f Hz with %i samples'
print(msg%(eff_f2, NSamples))

print('checks for f2:')
print('  %i sig2 cycles takes %.3f ms'%(mode, 1e3*(1/eff_f2)*mode))
print('  symbol ends after:  %.3f ms'%(1e3*NSamples/f_sample))
print('  audio sample each %.3f ms'%(1e3/f_sample))

print('''\nCut&Paste in Arduino code:
      
#define N_SAMPLES_WAVETABLE %i // %.3f ms each symbol
#define f1 %.2f // for BFSK, in Hertz 
#define f2 %.2f 
'''%(NSamples, 1e3*(1/eff_f2)*mode, eff_f1, eff_f2))

isamples = np.arange(NSamples)
w1 = 2*np.pi*eff_f1/f_sample
s1 = np.sin(w1*isamples)
w2 = 2*np.pi*eff_f2/f_sample
s2 = np.sin(w2*isamples)

last_points_shown = 400

time = 1e3*symbol_duration * isamples/NSamples

plt.hlines(0,11,20, color='black', linewidth=0.5)
plt.plot(time[-last_points_shown:], s1[-last_points_shown:], '.', color='blue', markersize=1)
plt.plot(time[-last_points_shown:], s2[-last_points_shown:], '.', color='red', markersize=1)
plt.xlabel("Time (ms)")
plt.show()




