#include <MicroNMEA.h>
#define WAITFORSERIAL false

#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
#define gps  Serial1
#define console Serial

#define VERSIONNUM 0 // two bits max
#define DAC_OUT_FREQ 48000 // in Hertz
#define DAC_AMPLITUDE 511
#define DAC_MID_VALUE 511 // (2**10 - 1) / 2
#define SQUARE_PULSE_DURATION 5 // ms
#define SQUARE_PULSE_LENGTH 1e-3*SQUARE_PULSE_DURATION*DAC_OUT_FREQ
#define SQUARE_HIGH (DAC_MID_VALUE + DAC_AMPLITUDE)
#define SQUARE_LOW DAC_MID_VALUE

// results pasted from ﻿FSKfreqCalculator.py (so each FSK symbol ends at y=0):
#define N_SAMPLES_WAVETABLE 960 // 20.000 ms each symbol
#define f1 600.00 // for BFSK, in Hertz 
#define f2 1150.00

volatile bool PPSarrived = false;
volatile bool DACtimeToWriteOut = false;
char nmeaBuffer[100];
char timecodeword[25] = {0};
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
volatile bool ppsTriggered = false; // flag to detect the external interrupt in loop()
uint16_t *wave_tables_f1_f2; // array to store sinewave points
int i_sample = 0;
void tcConfigure();
void tcENABLE();

void printUnknownSentence(MicroNMEA& nmea) {
  console.println();
  console.print("Unknown sentence: ");
  console.println(nmea.getSentence());
}

void TC5_Handler (void) {
  //  console.print("in tc5handler");
  DACtimeToWriteOut = true;
  TC5->COUNT16.INTFLAG.bit.MC0 = 1; //don't change this, it's part of the timer code
}

void PPSarrived_Handler (void) {
  DACtimeToWriteOut = true;
  PPSarrived = true;
  tcENABLE();
  analogWrite(A0, SQUARE_HIGH); // square pulse rising hedge
}

void punchGPSintoWord()
{
  int DD = nmea.getDay();
  int HH = nmea.getHour();
  int MM = nmea.getMinute();
  int SS = nmea.getSecond();
  nmea.clear();
  copybits_to_tcword(VERSIONNUM, 1, 2);
  copybits_to_tcword(SS, 3, 6);
  copybits_to_tcword(MM, 9, 6);
  copybits_to_tcword(HH, 15, 5);
  copybits_to_tcword(DD, 20, 5);
  //  for (int i = 0; i < 26; i++) {
  //    if (timecodeword[i]) console.print('1'); else console.print('O');
  //    console.print(timecodeword[i], BIN);
  //  }
  //  console.println();
  
  //  timecodeword[] binary content:
  //i0:  Sync Pulse (no value)
  //i1:  version num b0
  //i2:  version num b1
  //i3:  seconds b0 (LSB)
  //i4:  seconds b1
  //i5:  seconds b2
  //i6:  seconds b3
  //i7:  seconds b4
  //i8:  seconds b5
  //i9:  minutes b0 (LSB)
  //i10: minutes b1
  //i11: minutes b2
  //i12: minutes b3
  //i13: minutes b4
  //i14: minutes b5
  //i15: hours b0 (LSB)
  //i16: hours b1
  //i17: hours b2
  //i18: hours b3
  //i19: hours b4
  //i20: day b0 (LSB)
  //i21: day b1
  //i22: day b2
  //i23: day b3
  //i24: day b4
}

void setup() {
  Serial.begin(9600);
  while (!Serial and WAITFORSERIAL);
  gps.begin(9600); // gps
  //  Serial.println("in setup()"); Serial.println("SystemCoreClock"); Serial.println(SystemCoreClock);
  tcConfigure(); //configure the timer to run at <sampleRate>Hertz
  //  nmea.setUnknownSentenceHandler(printUnknownSentence);
  MicroNMEA::sendSentence(gps, PMTK_SET_NMEA_OUTPUT_RMCONLY);
  attachInterrupt(digitalPinToInterrupt(2), PPSarrived_Handler, RISING);
  analogWriteResolution(10); // set the DAC for 10 bits of resolution (max)
  wave_tables_f1_f2 = (uint16_t *) malloc(2 * N_SAMPLES_WAVETABLE * sizeof(uint16_t)); // Allocate the buffers where the samples are stored
  genSin();
}

void loop() {
  int DACvalue;
  if (PPSarrived) {
    //        console.println(nmea.getSecond());
    PPSarrived = false;
    //    delay(SQUARE_PULSE_DURATION);
    //    analogWrite(A0, 0);
    i_sample = 0; // sample 0 done in PPSarrived_Handler ....
    punchGPSintoWord(); // later?
  }
  if (DACtimeToWriteOut) {
    DACtimeToWriteOut = false;
    // getSampleValue();
    int i_symbol, DAC_value, i_wavetable;
    int bit_value;

    i_symbol = i_sample / N_SAMPLES_WAVETABLE; // sync = symbol 0
    i_wavetable = i_sample % N_SAMPLES_WAVETABLE;
    bit_value = timecodeword[i_symbol];
    DAC_value = wave_tables_f1_f2[2 * i_wavetable + bit_value];
    if (i_symbol == 0 && i_sample <= SQUARE_PULSE_LENGTH)// still in sync pulse
      DAC_value = SQUARE_HIGH; 
    if (i_symbol == 0 && i_sample > SQUARE_PULSE_LENGTH)// sync pulse finished
      DAC_value = SQUARE_LOW; 
    if (i_symbol == 24 && i_wavetable == (N_SAMPLES_WAVETABLE - 1)) { // last symbol and last sample
      tcDISABLE(); // stop triggering DAC outputs
      DAC_value = DAC_MID_VALUE;
    }
    analogWrite(A0, DAC_value);
    i_sample += 1;
  }

  while (!PPSarrived && gps.available()) {
    char c = gps.read();
    //    console.print(c);
    nmea.process(c);
  }
}

void tcConfigure() {
  GCLK->CLKCTRL.reg = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID(GCM_TC4_TC5)) ;
  while (GCLK->STATUS.bit.SYNCBUSY);
  tcReset(); //reset TC5
  TC5->COUNT16.CTRLA.reg |= TC_CTRLA_MODE_COUNT16; TC5->COUNT16.CTRLA.reg |= TC_CTRLA_WAVEGEN_MFRQ;
  TC5->COUNT16.CTRLA.reg |= TC_CTRLA_PRESCALER_DIV1 | TC_CTRLA_ENABLE;
  uint16_t TC5Val = (uint16_t) 999;
  //  uint16_t TC5Val = (uint16_t) 700;
  TC5->COUNT16.CC[0].reg = TC5Val;
  while (tcIsSyncing());
  NVIC_DisableIRQ(TC5_IRQn);
  NVIC_ClearPendingIRQ(TC5_IRQn);
  NVIC_SetPriority(TC5_IRQn, 0);
  NVIC_EnableIRQ(TC5_IRQn);
  TC5->COUNT16.INTENSET.bit.MC0 = 1;
  while (tcIsSyncing()); //wait until TC5 is done syncing
}

bool tcIsSyncing() {
  return TC5->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY;
}

void tcENABLE() {
  TC5->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE; //set the CTRLA register
  while (tcIsSyncing()); //wait until snyc'd
}

void tcReset() {
  TC5->COUNT16.CTRLA.reg = TC_CTRLA_SWRST;
  while (tcIsSyncing());
  while (TC5->COUNT16.CTRLA.bit.SWRST);
}

void tcDISABLE() {
  TC5->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;
  while (tcIsSyncing());
}

void genSin() {
  const float pi2 = 2 * 3.141592653; //2 x pi
  float phase1, phase2;

  for (int i = 0; i < N_SAMPLES_WAVETABLE; i++) { // loop to build sine wave based on sample count
    phase1 = pi2 * (float)i * f1 / DAC_OUT_FREQ; // calculate value in radians for sin()
    phase2 = pi2 * (float)i * f2 / DAC_OUT_FREQ;
    wave_tables_f1_f2[2 * i] = ((int)(sin(phase1) * DAC_AMPLITUDE + DAC_MID_VALUE)); // f1
    wave_tables_f1_f2[2 * i + 1] = ((int)(sin(phase2) * DAC_AMPLITUDE + DAC_MID_VALUE)); // f2
  }
}

void copybits_to_tcword(int val, int where, int nbits)
{
  for (int i = 0; i < nbits; i++) {
    timecodeword[where] = bitRead(val, i);
    where += 1;
  }
}
